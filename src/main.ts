import Vue from 'vue';
import App from './App.vue';
import router from './router';
import { store } from './store';
import i18n from '@/languages/i18n';
import './registerServiceWorker';

// bootstrap
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.css';

//element UI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import locale from 'element-ui/lib/locale/lang/vi';
Vue.use(ElementUI, {
  size: 'small',
  // i18n: (key: any, value: any) => i18n.t(key, value),
  locale
});

//FontAwesomeIcon
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faInfo } from '@fortawesome/free-solid-svg-icons';
Vue.component('font-awesome-icon', FontAwesomeIcon);
library.add(faInfo);

// event bus
import EventBus from 'vue-bus-ts';
Vue.use(EventBus);
const bus = new EventBus.Bus();

Vue.config.productionTip = false;

new Vue({
  i18n,
  router,
  store,
  render: (h) => h(App)
}).$mount('#app');
