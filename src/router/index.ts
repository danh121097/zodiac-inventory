import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Login from '@/views/login/login.vue';
import { store } from '@/store/index';
Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/home/home.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/zodiac-inventory',
    name: 'Zodiac Inventory',
    component: () => import('@/views/zodiac-inventory/zodiac-inventory.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/zodiac-inventory/:id',
    name: 'Zodiac Detail',
    component: () => import('@/components/zodiac-item/zodiac-item.vue'),
    meta: {
      requiresAuth: true
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.getters['auth/isAuthenticated'] === false) {
      let query = to.fullPath.match(/^\/$/) ? {} : { redirect: to.fullPath };
      next({
        path: '/login',
        query: query
      });
      return;
    }
  }
  next();
});

export default router;
