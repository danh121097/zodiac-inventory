import { Module } from 'vuex';
const Store: Module<any, any> = {
  namespaced: true,
  state: {
    token: ''
  },
  getters: {
    isAuthenticated: (state) => !!state.token
  },

  actions: {
    login({ commit }, token) {
      commit('AUTH_SUCCESS', token);
    }
  },

  mutations: {
    AUTH_SUCCESS(state, res) {
      state.token = res.data.token;
    }
  }
};

export { Store as auth };
