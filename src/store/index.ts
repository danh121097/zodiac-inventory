import Vue from 'vue';
import Vuex from 'vuex';
import { auth } from './modules/auth';
import createPersistedState from 'vuex-persistedstate';
import SecureLS from 'secure-ls';

const SecretKey = '';

const ls = new SecureLS({
  encodingType: 'aes',
  isCompression: false,
  encryptionSecret: SecretKey
});

const dataState = createPersistedState({
  paths: ['auth'],
  storage: {
    getItem: (key) => ls.get(key),
    setItem: (key, value) => ls.set(key, value),
    removeItem: (key) => ls.remove(key)
  }
});

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth
  },
  plugins: [dataState]
});

export { store };
