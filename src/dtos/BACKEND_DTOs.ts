import * as API_LINK from '@/dtos/API_CONST';
import request from '@/dtos/REQUEST';
const req = new request();

export default class BACKENDDTOs {
  public Authentication(params: any, callback: any) {
    const data = {
      mobile_number: params[0],
      password: params[1]
    };
    req.Authentication(API_LINK.Authentication, data, callback);
  }

  public GetListInventory(token: any, calllback: any) {
    req.getAjax(API_LINK.GetListInventory, token, calllback);
  }
}
