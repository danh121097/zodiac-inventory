import { Vue } from 'vue-property-decorator';
import $ from 'jquery';

export default class Req extends Vue {
  public Authentication(url: string, data: any, callback: any) {
    $.ajax({
      method: 'POST',
      url,
      headers: { 'Content-Type': 'application/json' },
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      data: JSON.stringify(data),
      success: callback,
      error: callback
    });
  }

  public getAjax(url: any, token: any, callback: any) {
    const request = $.ajax({
      method: 'GET',
      url,
      beforeSend: (xhr) => {
        xhr.setRequestHeader('Authorization', 'Bearer ' + token);
      },
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      success: callback,
      error: callback,
      timeout: 300000
    });
    return request;
  }

  public postAjax(url: string, token: any, data: any, callback: any) {
    $.ajax({
      method: 'POST',
      url,
      beforeSend: (xhr) => {
        xhr.setRequestHeader('Authorization', 'Bearer ' + token);
      },
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      data: JSON.stringify(data),
      success: callback,
      error: callback
    });
  }
}
