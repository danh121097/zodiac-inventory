import Vue from 'vue';
import VueI18n from 'vue-i18n';
import vnLang from '@/languages/vn.json';
import enLang from '@/languages/en.json';

Vue.use(VueI18n);

const messages = {
  vn: vnLang,
  en: enLang
};

const i18n = new VueI18n({
  locale: 'vn',
  messages,
  fallbackLocale: 'vn'
});

export default i18n;
