import { Component, Vue } from 'vue-property-decorator';
import DTOs from '@/dtos/BACKEND_DTOs';

@Component({
  components: {}
})
export default class ComponentBase extends Vue {
  //-------Declare DTOs------
  public DTOs = new DTOs();
  //---------------------
}
