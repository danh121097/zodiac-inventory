import ViewBase from '@/mixin/view-mixin';
import { Component, Mixins } from 'vue-property-decorator';

@Component({
  name: 'Home',
  components: {}
})
export default class Home extends Mixins(ViewBase) {}
