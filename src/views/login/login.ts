import { Component, Mixins } from 'vue-property-decorator';
import ViewBase from '@/mixin/view-mixin';

@Component({
  name: 'login',
  components: {}
})
export default class Login extends Mixins(ViewBase) {
  public loginModel: any = {
    mobileNumber: '',
    Password: ''
  };

  mounted() {
    this.loginModel.mobileNumber = '+6588888888';
    this.loginModel.Password = 'Hyz@dev123';
  }

  public submitLogin() {
    const n = this.loginModel.mobileNumber;
    const p = this.loginModel.Password;
    const data = [n, p];
    this.DTOs.Authentication(data, (res: any) => {
      this.$store.dispatch('auth/login', res);
      this.$router.push('/zodiac-inventory');
    });
  }
}
