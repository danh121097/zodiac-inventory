import ViewBase from '@/mixin/view-mixin';
import { Component, Mixins } from 'vue-property-decorator';
import { State } from 'vuex-class';
@Component({
  name: 'ZodiacInventory',
  components: {}
})
export default class ZodiacInventory extends Mixins(ViewBase) {
  @State('token', { namespace: 'auth' }) public token: any;
  listInventory: any = '';
  role: String = 'Animal';
  options: Array<{ role: string; label: string }> = [
    {
      role: 'Animal',
      label: 'Animal'
    },
    {
      role: 'Element',
      label: 'Element'
    },
    {
      role: 'Captivity',
      label: 'Time in Captivity'
    }
  ];

  created() {
    this.getListInventory();
  }

  getListInventory() {
    const token = this.token;
    this.DTOs.GetListInventory(token, (res: any) => {
      this.listInventory = res.data;
    });
  }
}
