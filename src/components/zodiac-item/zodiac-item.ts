import ComponentBase from '@/mixin/component-mixin';
import { Component, Mixins } from 'vue-property-decorator';
import { State } from 'vuex-class';

@Component({
  name: 'ZodiacItem',
  components: {}
})
export default class ZodiacItem extends Mixins(ComponentBase) {
  @State('token', { namespace: 'auth' }) public token: any;

  firstName: string = '';
  lastName: string = '';
  mounted() {
    this.getListInventory();
  }

  getListInventory() {
    const token = this.token;
    this.DTOs.GetListInventory(token, (res: any) => {
      let data = res.data;
      let id: any = this.$route.params.id;
      if (id) {
        data.filter((ele: any) => {
          if (ele._id === id) {
            this.firstName = ele.zodiac;
            this.lastName = ele.element;
          }
        });
      }
    });
  }
  back() {
    this.$router.go(-1);
  }
}
